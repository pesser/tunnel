## MathSim ssh access via tunnel

### Generate ssh key

    sh-keygen -t rsa -b4096 -f ~/.ssh/id_mathsim


### Add keys
Send `Manfred Trunk <manfred.trunk@iwr.uni-heidelberg.de>` your
public key `~/.ssh/id_mathsim.pub` to give you tunnel access on
`simaccess.iwr.uni-heidelberg.de`.
Furthermore add the content of the public key to
`~/.ssh/authorized_keys` on the machines you want to connect to.


### Set up ssh config
In `~/.ssh/config`

    Host mathsimtunnel
        User yourIWRusername
        Hostname simaccess.iwr.uni-heidelberg.de
        Port 42022
        IdentityFile ~/.ssh/id_mathsim
        LocalForward 55554 simserv01.iwr.uni-heidelberg.de:42022
        LocalForward 55555 simserv02.iwr.uni-heidelberg.de:42022

    Host simserv01
        User yourIWRusername
        Hostname localhost
        Port 55554
        IdentityFile ~/.ssh/id_mathsim

    Host simserv02
        User yourIWRusername
        Hostname localhost
        Port 55555
        IdentityFile ~/.ssh/id_mathsim


### Useage
Open the tunnel, connect, work and close the tunnel

    ./tunnel mathsimtunnel open
    ssh simserv01
    ...
    ./tunnel mathsimtunnel close
