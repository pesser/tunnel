#!/usr/bin/env bash

# helper script to open and close tunnels properly
# calling it without arguments shows all currently open tunnels
# otherwise specify the tunnel's host and whether to open or close it

socketname () {
  echo "~/sshs_$1"
}

socketnames () {
  echo $(find ~ -maxdepth 1 -name "sshs_*")
}

statuscheck () {
  for socket in $(socketnames)
  do
    echo $socket
    ssh -S $socket -O check dummy
  done
}

open () {
  echo "Opening $1";
  tunnel="$1"
  if ssh -MS $(socketname $tunnel) -f -N "$tunnel"
  then
    echo "Opened $tunnel"
  else
    echo "Failed to open $tunnel"
  fi
}

close () {
  echo "Closing $1";
  tunnel="$1"
  if ssh -S $(socketname $tunnel) -O exit "$tunnel"
  then
    echo "Closed $tunnel"
  else
    echo "Failed to close $tunnel"
  fi
}

if [ $# -eq 0 ]
then
  statuscheck
elif [ $# -ne 2 ] || [ "$2" != "open" -a "$2" != "close" ]
then
  echo "Useage: $0 <tunnel> [open|close]"
fi

if [ "$2" == "open" ]
then
  open "$1"
elif [ "$2" == "close" ]
then
  close "$1"
fi
